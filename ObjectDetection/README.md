# Object Detection
Ported from
[using DIGITS to train an Object Detection network](https://github.com/gheinrich/DIGITS/tree/master/examples/object-detection).

Follow the instructions in the [lab guide](INSTRUCTIONS.md).
