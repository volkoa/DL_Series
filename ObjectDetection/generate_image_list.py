#!/usr/bin/env python
'''
@author: Alex Volkov
@email: avolkov@nvidia.com
'''

# ------------------------------------------------------ Python Standad Library
import sys
import os
from argparse import (
    ArgumentDefaultsHelpFormatter, HelpFormatter, RawDescriptionHelpFormatter,
    ArgumentParser, SUPPRESS, FileType)
from textwrap import dedent

from string import split as strsplit
from functools import partial
import glob

from itertools import chain
import random
# import traceback

# ------------------------------------------------ module functions and Classes


class SmartFormatterMixin(HelpFormatter):
    # ref:
    # http://stackoverflow.com/questions/3853722/python-argparse-how-to-insert-newline-in-the-help-text
    # @IgnorePep8

    def _split_lines(self, text, width):
        # this is the RawTextHelpFormatter._split_lines
        if text.startswith('S|'):
            return text[2:].splitlines()
        return HelpFormatter._split_lines(self, text, width)


class CustomFormatter(ArgumentDefaultsHelpFormatter,
                      RawDescriptionHelpFormatter, SmartFormatterMixin):
    '''Convenience formatter_class for argparse help print out.'''


def _parser(desc):
    parser = ArgumentParser(description=dedent(desc),
                            formatter_class=CustomFormatter)

    parser.add_argument('-r', '--recursive', default=0, type=int,
                        help='S|Recursive depth level into input-dir.')

    parser.add_argument('-i', '--input-dir', required=True, default=SUPPRESS,
                        help='The directory containing image files. (Required)'
                        )

    parser.add_argument(
        '-n', '--nimages', default=10, type=int,
        help='S|Number of images to select randomly. Specify -1 for all.')

    parser.add_argument(
        '--fmt', default='png', type=partial(strsplit, sep=','),
        help='S|The image file format extension. '
        'Could be a comma separated list.\n')

    parser.add_argument(
        '-o', '--outfile', default='-', type=FileType('w'),
        help='S|Output text file with a list of images or print to stdout.\n'
        'One line per image path.')

    args = parser.parse_args()

    return args


def flatlist(iterable):
    return list(chain.from_iterable(iterable))


def main(argv=None):
    '''
    Generate a list of images in a text file. One line per image path. Ex.:
        $ ./{} --input-dir path_to_dir_with_images -o $PWD/imagelist.txt
    Used to generate list of images for the DIGITS demo example:
        https://github.com/gheinrich/DIGITS/tree/master/examples/object-detection
    '''
    argv = sys.argv if argv is None else sys.argv.extend(argv)
    desc = main.__doc__.format(os.path.basename(__file__))

    # CLI parser
    args = _parser(desc)

    rlevel = args.recursive
    input_dir = args.input_dir
    nimages = args.nimages
    fmt = args.fmt  # list of file formats even if just one format.
    outfile = args.outfile

    # print 'in: {}, out: {}'.format(input_dir, outfile)
    # print 'fmt: {}'.format(fmt)

    strpat = '{}/*.{}'
    filelist = flatlist(
        [glob.glob(strpat.format(input_dir, ifmt)) for ifmt in fmt])

    for _ in range(rlevel):
        for dirname in [os.path.join(input_dir, idir)
                        for idir in os.listdir(input_dir)
                        if os.path.isdir(os.path.join(input_dir, idir))]:
            filelist += flatlist(
                [glob.glob(strpat.format(dirname, ifmt)) for ifmt in fmt])

    if nimages == -1:  # special case. All images specified.
        pass
    elif nimages > 0:  # default is 10 images.
        nfiles = len(filelist)
        if nfiles < nimages:
            raise ValueError('Desired number of images {} greater than {} '
                             'images found'.format(nimages, nfiles))
        filelist = random.sample(filelist, nimages)
    else:
        return

    with outfile as of:
        for filepath in filelist:
            print >> of, filepath


if __name__ == '__main__':
    main()

