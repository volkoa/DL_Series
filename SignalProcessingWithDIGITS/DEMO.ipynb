{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Weak Signal Detection Using Deep Learning\n",
    "\n",
    "### Introduction\n",
    "When monitoring radio frequency (RF) signals, or similar signals from sensors such as biomedical, temperature, etc., we are often interested in detecting certain signal “markers” or features. This can become a challenging problem when the signal-of-interest is degraded by noise. Traditional signal detection methods use a range of techniques such as energy detection, “matched filtering”, or other correlation-based processing techniques using the collected time-series data. Short-duration radio frequency (RF) events can be especially challenging to detect, since the useful data length is finite and long integration times are not possible. Weak signals that are short in duration are some of the most difficult to reliably detect (or even find). In this short tutorial, we walk you through an approach based on using a Convolutional Neural Network (CNN) to tackle the traditional signal processing problem of detecting RF signals in noise.\n",
    "\n",
    "Signal detection theory often assumes that a signal is corupted with additive white Gaussian noise (AWGN). This type of noise is common in the real world and the assumption makes mathematical analysis tractable due to the properties of the noise process. The detection of a signal in noise depends on the signal duration, amplitude, and the corresponding noise process. This becomes more difficult if correlated noise, or interfering signals, are also in the same band as the signal you wish to detect.\n",
    "\n",
    "In this tutorial, we will assume no a-priori information about the signal-of-interest. As input to the Convolutional Neural Network, we will utilize spectrograms computed from simulated Radio Frequency (RF) data using a common Fast Fourier Transform (FFT) based method. Taking the input data into the frequency domain as time-frequency grams, which are 2D representations just like a picture, allows us to visualize the energy of a signal over some pre-determined time duration and frequency bandwidth. In practice, collecting and computing spectrograms over time, allows us to create a spectral monitoring system that can automatically monitor frequency bands for a signal-of-interest.\n",
    "\n",
    "For a single sinusoid in AWGN, finding the frequency bin with the maximum amplitude is a method for estimating signal frequency in a spectrogram. But real-world signals are often more complex, with frequency components that change with time, and creating a generalized signal detection algorithm becomes difficult. In this tutorial, we will look at one of these types of signals - Linear Frequency-Modulated (LFM) signals. In a follow-on tutorial we will explore Frequency-Hopped (FH) signals and multi-signal detection scenarios.\n",
    "\n",
    "### Linear Frequency-Modulated Signals\n",
    "One classic example, is the detection of a linear frequency-modulated (LFM), or chirp, signal. This is a signal that ramps up or down in frequency over some time frame. Its frequency changes with time based on its chirp rate. Chirps are used in many different systems for frequency response measurements and timing. RADAR systems use chirp signals due to the inherent large time-bandwith product available with coherent processing. Another common use is for automatic room equalization in home theater receivers, since chirps can excite a large frequency swath quickly. Chirps can also be used as “pilot” signals to denote the start of an incoming transmission, and more.\n",
    "\n",
    "Figure 1 shows a high-SNR chirp as seen in a grayscale spectrogram (the format we will be using). Since the spectrogram consists of real numbers all > 0, we can map it to a picture file by scaling the values appropriately. So we only need a single grayscale image channel. In this plot, the x axis is time and the y axis is frequency. Brightness is proportional to signal power.\n",
    "\n",
    "<img src=\"images2/figure2.png\" alt=\"Drawing\" style=\"width: 400px;\"/>\n",
    "<div align=\"center\">Fig1. High-SNR chip spectrogram (grayscale).</div>\n",
    "\n",
    "The above chirp (Figure 1) has a high SNR and is easy to detect with traditional signal processing algorithms. But when you are monitoring RF environments that contain other “offending” signals and high noise levels, reliable detection becomes more difficult. For example, Figure 2 shows an example spectrogram with some pulsed carrier waves (sinusoids) and a low-bitrate digital communication BPSK signal embedded in noise. Note, that this collect is over a 4 second window.\n",
    "\n",
    "<img src=\"images2/figure3.png\" alt=\"Drawing\" style=\"width: 400px;\"/>\n",
    "<div align=\"center\">Fig2. Typical of real-world noisy spectrum (x-axis is time, y-axis is frequency).</div>\n",
    "\n",
    "In this spectrogram there is no chirp signal, just noise and other comms-like signals. This is similar to what “real-world” RF signals look like – combinations of signal classes with different strengths, all embedded in noise. As an exemplar of the problem we will solve, Figure 3 consists of another spectrogram showing noise, interfering signals, and a weak chirp signal.\n",
    "\n",
    "<img src=\"images2/figure4.png\" alt=\"Drawing\" style=\"width: 400px;\"/>\n",
    "<div align=\"center\">Fig3. Weak chirp embedded in noise (x-axis is time, y-axis is frequency).</div>\n",
    "\n",
    "In Figure 3 the chirp signal is 7 dB below the noise power in this frequency band. That is, the signal-to-noise-ratio (SNR) for the chirp is -7 dB. It is barely visible to the human eye. Traditional detection methods, without large amounts of integration and/or a prior signal model, fail consistently in detecting a weak signal like this. Moreover, since we have interfering signals that are sharing the same bandwidth as the chirp, the problem becomes even harder.\n",
    "\n",
    "When monitoring RF signals, we want accurate detection of these types of signals, as a human cannot visually inspect all the data manually. For example, in the case of intelligent spectral monitoring or cognitive radio, we want something to autonomously analyze extraordinary amounts of signal data all the time. The question arises: Can we design a better process to help detect these weak signals?\n",
    "\n",
    "\n",
    "# Deep Spectral Detection: Data and Network Creation\n",
    "- We will create a two-output convolutional neural network that ingests an image of a time/frequency signal spectrogram. The network will determine whether a chirp signal is present (class 0 - signal) or a chirp signal is NOT present (class 1 - noise).\n",
    "\n",
    "### Starting up DIGITS\n",
    "- We have a pre-configured DIGITS instance set up for use with this tutorial. Click [`HERE`](http://ec2-54-144-82-210.compute-1.amazonaws.com:5000) to start up an instance.\n",
    "\n",
    "- The digits server page should be active. Now we will create the dataset using the gui tools in digits.\n",
    "\n",
    "### Creating DIGITS image database\n",
    "- Goto the left side and click the 'Images' button underneath the 'New Dataset' label. Select the 'Classification' menu option.\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot1.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- Select grayscale image type, keep the image size 256x256 and enter the directory where the dataset files are stored. The settings used are shown in the below pic.\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot2.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- Name the dataset and click \"create\". The data gen screen will show up\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot3.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- You can wait for the database gen to finish. Then click on the upper left \"DIGITS\" text to go back to the digits main screen. You should see your new dataset at the top. If you click on the dataset again, you will now see the processed data set, the mean image it computed, and links to explore images within the database:\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot4.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "### New CNN model creation\n",
    "- Go back to the digits main page and goto the right side and click the 'Images' button underneath the 'New Model' label. Select the 'Classification' menu option.\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot5.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- In the New Image Classification Model screen, select the image database to use in the upper left.\n",
    "    - For the model to use, select 'Custom Network' and paste the contents of the following [chirp CNN prototxt](http://datasets.kickview.com:8080/dsd_demo/train_val_digits4_chirp.prototxt) into the window.\n",
    "    - Select Nesterov in the solver type menu and set the learning rate (LR) to 0.001.\n",
    "    - Also click on the Advanced button in the learning tab and select \"Exponential decay\"\n",
    "    - Note we set the validation epoch to 1 so the network validation will be run once every epoch.\n",
    "    - We train for only 5 epochs\n",
    "    \n",
    "<img src=\"images2/Digits_Screenshot6.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- You can also click on the \"Visualize\" button next to the custom network window. This will show you what the network looks like below.\n",
    "    - We started with an AlexNet and pruned the number of fully-connected layers to 2.\n",
    "    - The two fully connected layers were also reduced in size (less neurons).\n",
    "    - Added regularization to aid in a better-trained network.\n",
    "    \n",
    "<img src=\"images2/Vis_Screenshot.png\" alt=\"Drawing\" style=\"width: 400px;\"/>\n",
    "\n",
    "- Finally name the model and click \"Create\". The training screen will show up next with the ongoing training plots.\n",
    "    - In the below plot, you can see the network is unable to learn. The loss curve never decreases.\n",
    "    - This is part of the hyperparameter search space that needs to be explored in order to get a model to train well. We will iterate on the model next.\n",
    "    \n",
    "<img src=\"images2/Digits_Screenshot7.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- After training is complete, click the \"Clone job\" button at the top right. This will create an exact copy of the model for iterating.\n",
    "    - For this experiment, increase the learning rate to 0.008.\n",
    "    - Number of epochs is upped to 7\n",
    "    - Click create (Digits will allow the same model name)\n",
    "    \n",
    "<img src=\"images2/Digits_Screenshot8.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- CONGRATULATIONS, you have trained your Convolutional Neural Network! Digits saves a copy of the network model at each epoch (it's one of the training parameters), so we can go back and analyze any epoch of the training process.\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot9.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "### What can I do with this thing?\n",
    "- Now, you can test it out on a couple training images just for fun. The classification accuracy will be high, but this is just an quick example.\n",
    "- Go to the bottom of the model page and click on the 'Upload image' in the 'Test a Single Image' section. Although it's not a good practice in general to test with data from the training set, let's select one of the images used in training just to see the visualization features available in Digits.\n",
    "\n",
    "<img src=\"images2/Digits_Screenshot10.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "- Next, check the 'Show visualizations and statistics' box. Then click the 'Classify One' button.\n",
    "- The model is run using the image with the classification results up top. The bottom plots show the activations in each layer of the network.\n",
    "- Here we see the network classifies correctly with a probability of 99.98%.\n",
    "\n",
    "<img src=\"images2/Screenshot13.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "# Neural Network Generalization on New Signals\n",
    "\n",
    "### Test Set \n",
    "- A test data set is provided with 500-1000 positive and negative examples The test set will be used to determine the generalization cability of the trained network.\n",
    "- A range of SNR values are used in the test set in order to analyze the network's ability to discriminate low and high SNR signals. In doing so, you can come up with a probability of detection (PD) and a probability of false alarm (PFA) metrics for subset of signal strengths\n",
    "- We wont be using digits here! We will run a python script.\n",
    "\n",
    "### Download saved network model\n",
    "- Go back to Digits and the network model we just trained. Under the 'Trained Models' section, you can download the network for each epoch time that Digits saved off the network weights. If the model trained well, you would typically just download from the last epoch.\n",
    "- For this tutorial we have downloaded the model and prepared it for you. The path to the model is /home/ubuntu/demo/model\n",
    "\n",
    "<img src=\"images2/Screenshot_model.png\" alt=\"Drawing\" style=\"width: 600px;\"/>\n",
    "\n",
    "### Python analysis script\n",
    "##### For this tutorial we have already pre-installed the model and test images into Digits, the model is located at /home/ubuntu/demo/model \n",
    "\n",
    "- The file 'analyze_spect_dir.py' in the gitlab repo that was downloaded previously is what we will be using. It has `two functions, detection_tst( pos_file_dir, model_dir, pic_type) and false_alarm_tst( neg_file_dir, model_dir, pic_type)`\n",
    "- The functions take in arguments: positive/negative image file directory, caffe model directory, pic type ('jpg' or 'png')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import analyze_spect_dir as ansp\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "- Now we will run the detection test, which runs all the positive class files through the caffe network model and tallies the detection rate (signal present and detected) and the miss rate (signal present but not detected)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "ansp.detection_tst('/home/ubuntu/demo/testdemo/pos', '/home/ubuntu/demo/model', 'jpg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "- After the above has completed you can see that the network has a decent detection probabilty, but all the cases it cannot detect have SNR = -5 dB!\n",
    "- We can fine tune the fully-connected layers of the network to do better at lower SNRs.\n",
    "- Next run the false alarm test. Here we input the negative test examples and see how many of them trigger a positive classification of a chirp present (i.e. a false detection)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "ansp.false_alarm_tst('/home/ubuntu/demo/testdemo/neg', '/home/ubuntu/demo/model', 'jpg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "- The PFA of the classifier is << 1% !\n",
    "- Now you have a way to determine the classification strength of newly trained networks (of this type)!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "### Fine tuning the network model\n",
    "- One way to fine tune the model is to create another training set with very low SNRs, around -4 to -8 dB.\n",
    "- Copy this image training set over and make a new database in Digits for this low SNR dataset.\n",
    "- Clone the trained classification model in digits so we can train it again using the new dataset.\n",
    "- In the \"Pretrained model\" window, you can enter the snapshot .caffemodel that you saved off from the initial training. This serves as the initialization of the network which should already have good weights.\n",
    "- Make the learning rate much smaller (/10 or /100) so the network cannot unlearn too much.\n",
    "- Train on the low SNR data set for a handful of epochs. See if the network is able to learn more about low SNR cases.\n",
    "- After training, you would import your new dataset and test it using ansp.detection_tst, for this tutorial we have done this for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "ansp.detection_tst('/home/ubuntu/demo/testdemo/pos', '/home/ubuntu/demo/model/finetunelow', 'jpg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "- With the tuned model, the network improves slightly on classifying low-SNR chirp signals (About 30%) while still classifying correctly on higher SNR examples.\n",
    "- However, this secondary training with low-SNR also decreased the false-alarm rate (from 2.5% to 1.5)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "deletable": true,
    "editable": true
   },
   "outputs": [],
   "source": [
    "ansp.false_alarm_tst('/home/ubuntu/demo/testdemo/neg', '/home/ubuntu/demo/model/finetunelow', 'jpg')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": true,
    "editable": true
   },
   "source": [
    "- To temper the network changes, we would need to\n",
    "    - train with more noise examples\n",
    "    - freeze the CNN layer weights during this fine tuning so the network does not change too much.\n",
    "- Here the secondary training has made too much of a swing.\n",
    "- Although note that the network performance is asymmetric, as we gained 14% of detection probability in trade for 10% of false alarm rate with this simple example."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
