# Signal Procesing using DIGITS

Start by extracting the [demo.tar.gz](demo.tar.gz) contents.
```bash
tar -xzvf demo.tar.gz -C digits_workdir/
```

Then follow the instructions in python notebook [DEMO.ipynb](DEMO.ipynb).

