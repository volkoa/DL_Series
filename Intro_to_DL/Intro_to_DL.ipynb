{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Deep Learning on GPUs\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin, let's verify [WebSockets](http://en.wikipedia.org/wiki/WebSocket) are working on your system with this notebook.  To do this, execute the cell block below by giving it focus (clicking on it with your mouse), and hitting Ctrl-Enter, or pressing the play button in the toolbar above.  If all goes well, you should see some output returned below the grey cell.  If not, please consult the [Self-paced Lab Troubleshooting FAQ](https://developer.nvidia.com/self-paced-labs-faq#Troubleshooting) to debug the issue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print \"The answer should be three: \" + str(1+2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's execute the cell below to display information about the GPUs running on the server."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "nvidia-smi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this class we will introduce the topic of Deep Learning, a rapidly growing segment of Artificial Intelligence.  Deep Learning is increasingly being used to deliver near-human level accuracy in image classification, voice recognition, natural language processing, and more.  In this class we will cover the basics of Deep Learning through some live examples, we will introduce the three major Deep Learning software frameworks and demonstrate why Deep Learning excels when run on GPUs.\n",
    "\n",
    "This introductory class is intended to serve as a first introduction to the concept of Deep Learning and a live tour of the major software frameworks.  There is some complex looking code presented, but it is not necessary to understand this code to complete the class.  There are some times where you will be waiting a couple of minutes for the Deep Learning algorithms to run - feel free to use this time to explore the code.\n",
    "\n",
    "By the end of this class you will hopefully be excited by the potential applications of Deep Learning and have a better idea of which of the frameworks you may want to learn more about in one of our [upcoming follow-on classes](https://developer.nvidia.com/deep-learning-courses)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Demo\n",
    "\n",
    "We will begin with a demonstration that will use a neural network to identify the subject of an image. Set the path below to the image file from your local machine."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "\n",
    "images_upload_dir = os.path.join(os.getcwd(), 'Intro_to_DL_files', 'images')\n",
    "your_image_name = \"bee.jpg\"\n",
    "# your_image_name = \"Koala.jpg\"\n",
    "# your_image_name = \"Penguins.jpg\"\n",
    "# your_image_name = \"Desert.jpg\"\n",
    "image = os.path.join(images_upload_dir, your_image_name)\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from PIL import Image\n",
    "im = Image.open(image)\n",
    "plt.imshow(im)\n",
    "plt.axis('off')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the image is displayed above, execute the cell below to get some predictions about what the image is showing. You need to install [Overfeat](http://cilvr.nyu.edu/doku.php?id=software:overfeat:start#installation) and set the variable OVERFEAT_PATH below, otherwise skip the next step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%%bash -s \"$image\"\n",
    "OVERFEAT_PATH=\"\" # set path to where overfeat is installed. Optional.\n",
    "LD_LIBRARY_PATH+=:${OVERFEAT_PATH}/lib PATH+=:${OVERFEAT_PATH}/bin/linux_64 overfeat -l -n 10 $1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What you see as output are 10 predictions (in order) about the contents of the image produced by a Deep Learning (DL) system called [Overfeat](http://cilvr.nyu.edu/doku.php?id=code:start). Each prediction is of an object which may be present in the image along with the confidence in the predictions.  In machine learning we call these predictions 'classifications'.\n",
    "\n",
    "If you were tasked with writing a computer program to make predictions like this, how would you do it?\n",
    "\n",
    "The traditional approach to this problem taken by the computer vision research community was to hand-craft functions which would look for particular features in the image that were believed to be indicative of certain objects or scenes.  For example, hard corners and straight edges might be believed to indicate the presence of manmade objects in the scene.  The responses from these feature extraction functions would then be fed into another function which would decide whether to declare a particular object had been detected in the image.\n",
    "\n",
    "![Traditional approach](files/Intro_to_DL_files/tradition.png)\n",
    "\n",
    "<div align=\"center\">*Figure 1: Traditional approach to machine perception - hand-crafted features are extracted from the raw data and is then independently passed to a classification function. This typically leads to brittle and false-alarm prone detectors.*</div>\n",
    "\n",
    "Unfortunately there are a number of problems with this approach.  Firstly, it is very hard to think of robust, reliable features which map to specific object types.  Secondly, it is a massive task to come up with the right combination of features for every type of object you want to be able to classify.  Thirdly, it is very difficult to design functions that are robust to translations, rotations and scalings of objects in the image.  Together these problems resulted in traditional computer vision struggling to develop high accuracy object detectors and classifiers for a broad range of objects.\n",
    "\n",
    "The DL system that you saw demonstrated above was created in a very different way.  No human knowledge was encoded in the DL model about the types and combinations of features that are important for labelling different types of object or scene.  Instead, a combined feature extraction and classification model was learned by allowing the computer to examine millions of ground-truth labelled images to discover which features and combinations of features were most discriminative for each of the object classes (In machine learning we call this 'training' the model).  Furthermore, this was done in such a way that the model didn't just learn to classify the specific objects it was trained on; instead, it abstracted out the essence of those objects in such a way that it could recognize previously unseen but visually similar objects.  This learning process refined tens of millions of free parameters in the DL model above to be able to accurately classify 1000 different object types.  On a traditional CPU based server this training would take weeks to complete.  This network was instead trained in just hours by exploiting GPU acceleration.  Almost all DL systems today exploit massive GPU acceleration to make training practical.\n",
    "\n",
    "![Machine learning approach](files/Intro_to_DL_files/DL.png)\n",
    "\n",
    "<div align=\"center\">*Figure 2: In the Deep Learning approach the feature extraction and classification functions are simultaneously learned using large amounts of training data.  The learned model can then be deployed in a new workflow where previously unseen data samples are classified.*</div>\n",
    "\n",
    "In this class we are going to take a tour of the technology behind DL and the popular open-source frameworks which make it accessible to researchers and software developers today.  Hopefully by the end of the class you will have an appreciation for the varied applications of DL, the benefits of some of the available DL frameworks and the essential nature of GPU acceleration for training real-world networks in reasonable timeframes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is Deep Learning?\n",
    "\n",
    "Deep Learning (DL) is a branch of artificial intelligence research that is attempting to develop the techniques that will allow computers to learn complex perception tasks such as seeing and hearing at human levels of performance.  Recent advances in DL have yielded startling performance gains in fields such as computer vision, speech recognition and natural language understanding.  DL is already in use today to understand data and user inputs in technologies such as virtual personal assistants and online image search.  DL is an active area of ongoing research where it is envisaged that human level perception of unstructured data will enable technologies such as self-driving cars and truly intelligent machines.\n",
    "\n",
    "DL attempts to use large volumes of unstructured data, such as images and audio clips, to learn hierarchical models which capture the complex structure in the data and then use these models to predict properties of previously unseen data.  For example, DL has proven extremely successful at learning hierarchical models of the visual features and concepts represented in handheld camera images and then using those models to automatically label previously unseen images with the objects present in them.\n",
    "\n",
    "The models learned through DL are biologically inspired artificial neural networks (ANNs).  An ANN is an interconnected group of nodes, akin to the vast network of neurons in a brain. In the image below, each circular node represents an artificial neuron and an arrow represents a connection from the output of one neuron to the input of another.  Input data is fed into the red nodes, and dependent on the weights on the connections between nodes, causes varying levels of activation of the subsequent hidden and output nodes.  In our example above the input nodes would be connected to image pixels and the output nodes would have a one-to-one correspondence with the possible object classes; the job of the hidden nodes is to learn the complex function which maps pixels to object labels. \n",
    "\n",
    "![ANN](files/Intro_to_DL_files/ANN.png)\n",
    "\n",
    "<div align=\"center\">*Figure 3:  Fully-connected Artificial Neural Network with one hidden layer*</div>\n",
    "\n",
    "For the advanced reader, the activation of a neuron is just a function of a variable which is the weighted sum of the inputs.  For basic neural networks the function is a sigmoid function.  The idea is that if the weighted sum of the inputs exceeds a threshold value, the neuron gives a positive output.\n",
    "\n",
    "For ANNs to be effective in difficult perception tasks, such as object labelling in images, these networks usually have many stacked layers of artificial neurons each with many neurons in the layer.  It is these many wide layers that lead to these networks being labelled Deep Neural Networks (DNNs).  \n",
    "\n",
    "One particular class of DNN which has shown great capability in visual perception tasks is called the Convolutional Neural Network (CNN).  CNNs have a structure which loosely resembles the structure the human visual cortex where lower levels of the model hierarchy are focused on small and local visual details, such as oriented line segments, which aggregate together into higher levels of the model which correspond to complex human concepts, such as faces and animals.\n",
    "\n",
    "![CNN](files/Intro_to_DL_files/cnn.png)\n",
    "\n",
    "<div align=\"center\">*Figure 4:  Deep Neural Networks learn a hierarchical model of the input data in which the layers correspond to increasingly complex real-world concepts.  The number of parameters in a network is a function of the number of neurons in the network and the architecture of the network connectivity.*</div>\n",
    "\n",
    "The [Imagenet Challenge](http://image-net.org/challenges/LSVRC/2014/index) is an annual competition where competitors are provided with 1.2 million natural images from the internet which are labelled with the objects that appear in those images using 1000 different class labels.  Competitors must create a model using this data which will then be tested against a further 100,000 images to see how accurately the model can identify and localize objects within them.  Over the past few years CNN based approaches have come to dominate the competition with accuracy in the object identification task recently exceeding 95% - which is comparable with human performance in labelling the objects in the test dataset.\n",
    "\n",
    "The mathematics that underpins DL training is predominantly linear matrix algebra.  Computation of this type of mathematics is highly parallelizable making it a perfect fit for acceleration using GPUs.  Training a DNN that can be competitive in the Imagenet challenge is computationally very intensive and would take weeks, months or even years of computation on a CPU based system.  Through massive parallelization, GPUs can reduce this training time to days or even hours. Now almost all entrants into the Imagenet Challenge use GPUs, sometimes many of them, to train CNNs with billions of trainable parameters.  The graph below shows how significant recent improvements in accuracy in the Imagenet challenge correlate with the explosion in the use of GPUs for training DNN entries.\n",
    "\n",
    "![](files/Intro_to_DL_files/gpuimagenet.png)\n",
    "\n",
    "<div align=\"center\">*Figure 5: The introduction of GPU accelerated Deep Learning into the ImageNet challenge began a period of unprecedented performance improvements.*</div>\n",
    "\n",
    "GPUs are not only far more computationally efficient for training DNNs - they are also far more cost effective at scale.  In 2013 Google built it's \"Google Brain\" - a 1000 server, 16,000 core CPU based cluster for training a state-of-the-art DNN for image understanding.  It cost an estimated \\$5,000,000 to build.  Shortly afterwards a team from the Stanford AI Lab showed that using 3 GPU accelerated servers with 12 GPUs per server - a total of 18,432 cores - they could train the same DNN and achieve the same performance.  Their system cost approximately $33,000 - 150th of the hardware cost and energy usage ([Wired Article](http://www.wired.com/2013/06/andrew_ng/)).\n",
    "\n",
    "![Google Brain vs. Stanford GPUs](files/Intro_to_DL_files/gbrain.png)\n",
    "\n",
    "<div align=\"center\">*Figure 6: GPUs are the most cost effective and size and power efficient means for training large Deep Neural Networks.*</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deep Learning Frameworks\n",
    "\n",
    "In recent years a number of software development frameworks have been created to make researching and applying DL more accessible and efficient.  In this class we will explore Caffe. Other popular frameworks are Theano, Torch, Tensorflow, CNTK, etc.  They all share the common benefits of providing highly optimized GPU enabled code specific to the computations required for training DNNs whilst providing access to that code through simple command line or scripting language interfaces such as Python.  Many powerful DNNs can be trained and deployed using these frameworks without ever having to write any GPU or complex compiled code, but whilst still benefiting from the huge training speed-up afforded by GPU acceleration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Framework 1: Caffe\n",
    "\n",
    "[Caffe](http://caffe.berkeleyvision.org/) is a DL framework created by PhD student Yangqing Jia from UC Berkley.  Some of benefits of Caffe to the DL practitioner and researcher:\n",
    "* Caffe is fast due to it's highly optimized C/CUDA backend which integrates GPU acceleration\n",
    "* Caffe is still very accessible due to command line, Python and Matlab interfaces  \n",
    "* The wrapper interfaces make it very easy to integrate DNN training and deployment into larger data analytics workflows  \n",
    "* Caffe has a large open-source development community adding new features all time\n",
    "* Caffe has an associated model-zoo where researchers can upload trained models for others to fine tune or use for inference using their own data\n",
    "\n",
    "You may choose to use Caffe as your DL framework if you fit the following profile:\n",
    "\n",
    "* You are interested in quickly getting up and running training and testing DNNs against your own image datasets without writing a lot of new code\n",
    "* You are a C/CUDA developer wishing to extend DL functionality within a framework\n",
    "\n",
    "One of the key benefits of Caffe for those wishing to apply deep learning to their own data is that defining and building many different types of DNN is possible without ever writing a line of code.  In this section we'll guide you through doing just that.\n",
    "\n",
    "#### Caffe example\n",
    "\n",
    "We're going to work with the well-known [CIFAR-10](http://www.cs.toronto.edu/~kriz/cifar.html) dataset which consists of 60000 32x32 color images split into ten classes: airplane, automobile, bird, cat, deer, dog, frog, horse, ship and truck.  Here are some examples:\n",
    "\n",
    "![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/airplane1.png)![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/automobile10.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/bird7.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/cat9.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/deer6.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/dog4.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/horse6.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/ship1.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/truck5.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/airplane4.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/automobile4.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/bird1.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/cat1.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/deer4.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/deer8.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/dog10.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/ship7.png) ![](http://www.cs.toronto.edu/~kriz/cifar-10-sample/horse4.png)\n",
    "\n",
    "We're working with the smaller CIFAR-10 dataset so that you can actually train a network from start to finish within the time available in this class; however, the CNN we're defining and training exhibits all the essential features of the much larger networks used in the ImageNet challenge.\n",
    "\n",
    "Training a DNN in Caffe requires you to provide three things: a [network definition file](files/Intro_to_DL_files/examples/cifar10/cifar10_quick.prototxt) specifying the neural network architecture, a [solver definition file](files/Intro_to_DL_files/examples/cifar10/cifar10_quick_solver.prototxt) specifying the training parameters and a dataset.  We've prepared each of these for you and there's no need to change them but you can take a look at them using the links provided.  The network and solver definition files are human readable text files.  Our dataset of images has been preloaded into a database to make it faster to read into Caffe.  \n",
    "\n",
    "In the network definition file we specify the inputs, outputs and structure of each of the layers in the DNN.  Here is an example of how a convolutional layer is defined, which is representative of how all layers are defined:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "layer {\n",
    "  name: \"conv1\"\n",
    "  type: \"Convolution\"\n",
    "  bottom: \"data\"\n",
    "  top: \"conv1\"\n",
    "  param {\n",
    "    lr_mult: 1\n",
    "  }\n",
    "  param {\n",
    "    lr_mult: 2\n",
    "  }\n",
    "  convolution_param {\n",
    "    num_output: 32\n",
    "    pad: 2\n",
    "    kernel_size: 5\n",
    "    stride: 1\n",
    "  }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The solver definition file is simply a list of parameters which are passed to the DNN training algorithm.  Here is how the parameters are set in this example:"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# The train/test net protocol buffer definition\n",
    "net: \"examples/cifar10/cifar10_quick_train_test.prototxt\"\n",
    "# test_iter specifies how many forward passes the test should carry out.\n",
    "# In the case of MNIST, we have test batch size 100 and 100 test iterations,\n",
    "# covering the full 10,000 testing images.\n",
    "test_iter: 100\n",
    "# Carry out testing every 500 training iterations.\n",
    "test_interval: 500\n",
    "# The base learning rate, momentum and the weight decay of the network.\n",
    "base_lr: 0.001\n",
    "momentum: 0.9\n",
    "weight_decay: 0.004\n",
    "# The learning rate policy\n",
    "lr_policy: \"fixed\"\n",
    "# Display every 100 iterations\n",
    "display: 100\n",
    "# The maximum number of iterations\n",
    "max_iter: 4000\n",
    "# snapshot intermediate results\n",
    "snapshot: 4000\n",
    "snapshot_prefix: \"examples/cifar10/cifar10_quick\"\n",
    "# solver mode: CPU or GPU\n",
    "solver_mode: GPU"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The training and test data corresponding to CIFAR-10, have been pre-populated into lmdb format. The sample code below illustrates how to explore the data within lmdb. Feel free to modify the code to explore and display the contents within lmdb."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import os\n",
    "import caffe\n",
    "import lmdb\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "lmdb_dirname = os.path.join(\n",
    "    os.getcwd(), 'Intro_to_DL_files/examples/cifar10')\n",
    "\n",
    "# USER SET/MODIFY ###########################################\n",
    "isample = 100\n",
    "lmdb_dir = os.path.join(lmdb_dirname, 'cifar10_train_lmdb')\n",
    "#lmdb_dir = os.path.join(lmdb_dirname, 'cifar10_test_lmdb')\n",
    "#############################################################\n",
    "\n",
    "datum = caffe.proto.caffe_pb2.Datum()\n",
    "with lmdb.open(lmdb_dir, readonly=True) as lenv:\n",
    "    cursor = lenv.begin().cursor() # txn = lmdb_env.begin()\n",
    "    lmdb_keys = [kk for kk in cursor.iternext(keys=True, values=False)]\n",
    "    # nsamples = txn.stat()['entries']\n",
    "    nsamples = len(lmdb_keys)\n",
    "    # if isample < nsamples:\n",
    "    try:\n",
    "        datum.ParseFromString(cursor.get(lmdb_keys[isample]))\n",
    "    except IndexError:\n",
    "        raise IndexError('sample index {} out of range of {} samples'\n",
    "                         .format(isample, nsamples))\n",
    "\n",
    "label = datum.label\n",
    "# List of class labels\n",
    "classes = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog',\n",
    "           'frog', 'horse', 'ship', 'truck']\n",
    "print 'Datum info: numbre of samples = {}'.format(nsamples)\n",
    "print 'label:cls = {}:{}, img. shape = {}'.format(\n",
    "    label, classes[label], (datum.channels, datum.height, datum.width))\n",
    "img = caffe.io.datum_to_array(datum).transpose(1, 2, 0)       \n",
    "    \n",
    "plt.imshow(img)\n",
    "plt.axis('off')\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The training process consists of first randomly initializing the weights on the connections between neurons in the network whose structure is defined in the network definition file.  Then, one at a time, each image in the training dataset is fed into the network along with its label.  The network guesses the label based purely on the input pixels - this is called classification.  If the guess is wrong then a small modification is made to the weights so that the network will guess more accurately for that image next time it sees it.  This process is repeated many thousands of times until the network converges to a stable average classification accuracy across all the training images.\n",
    "\n",
    "Training the model is as simple as running the cell below.  When you do so, you will see a large number of messages flying by.  First the network will be initialized layer by layer and then training will begin.  After each 100 training iterations you will get an update on the training loss which should be decreasing and after each 500 you will get an update on the accuracy against a test set (Test net output #0) which should be increasing.  An iteration is when the network trains on each image in the training set exactly once.  **Training the network takes a couple of minutes**, so spend the time familiarizing yourself with Caffe's output - keep scrolling to the bottom of the output window to see the latest updates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cd Intro_to_DL_files && \\\n",
    "caffe train --solver=examples/cifar10/cifar10_quick_solver.prototxt # -gpu 0,1,3,4\n",
    "# Use gpu flag for multi-GPU job. Note: solver_count == total Batch size must be divisible by the number of solvers (GPUs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Congratulations - you just trained your first DNN!  After 4000 iterations you will see the accuracy against the test set was ~71% - not bad for a couple of minutes training!  Each iteration took less than 0.02 seconds to train using the GPU.\n",
    "\n",
    "The learned network weights get saved into a binary file called cifar10_quick_iter_4000.caffemodel\n",
    "\n",
    "Classifying new images using the newly trained dataset is just as simple as training was - this time we will use Caffe's Python interface.  Activate the cell below as many times as you like to randomly choose an image from the test set and have it classified - there is no need to edit any of the code. Remember - the test set is images that were never used in training the network."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Import required Python libraries\n",
    "%matplotlib inline\n",
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import caffe\n",
    "from caffe.proto import caffe_pb2\n",
    "from caffe.io import blobproto_to_array\n",
    "import random\n",
    "\n",
    "# Choose network definition file and pretrained network binary\n",
    "basedir = os.path.join(os.getcwd(), 'Intro_to_DL_files')\n",
    "MODEL_FILE = os.path.join(basedir, 'examples/cifar10/cifar10_quick.prototxt')\n",
    "PRETRAINED = os.path.join(basedir, 'examples/cifar10/cifar10_quick_iter_4000.caffemodel')\n",
    "\n",
    "MEAN_FILE  = os.path.join(basedir, 'examples/cifar10/mean.binaryproto')\n",
    "blob = caffe_pb2.BlobProto()\n",
    "blob.ParseFromString(open(MEAN_FILE, \"rb\").read())\n",
    "mean = blobproto_to_array(blob).squeeze()\n",
    "\n",
    "# Load a random image\n",
    "x = caffe.io.load_image(basedir + '/examples/images/' + str(random.randint(1,18)) + '.png')\n",
    "\n",
    "# Display the chosen image\n",
    "plt.imshow(x)\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n",
    "# Load the pretrained model and select to use the GPU for computation\n",
    "caffe.set_mode_gpu()\n",
    "net = caffe.Classifier(MODEL_FILE, PRETRAINED,\n",
    "                       #mean=np.load(os.path.join(basedir, 'examples/cifar10/cifar10_mean.npy')).mean(1).mean(1),\n",
    "                       mean=mean,\n",
    "                       raw_scale=255,\n",
    "                       image_dims=(32, 32))\n",
    "\n",
    "# Run the image through the pretrained network\n",
    "prediction = net.predict([x])\n",
    "\n",
    "# List of class labels\n",
    "classes = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']\n",
    "\n",
    "# Display the predicted probability for each class\n",
    "plt.plot(prediction[0])\n",
    "plt.xticks(range(0,10), classes, rotation=45)\n",
    "# Display the most probable class\n",
    "print classes[prediction[0].argmax()]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even for this relatively small dataset and network architecture GPU acceleration is still highly desirable.  Running the cell below will train the same network for 100 iterations using the CPU - as you will see, it is considerably slower than training on the GPU was above. Training for 0.025 times as many iterations takes about 50% more time!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# CPU training does not work currently unless caffe is built with CPU_ONLY.\n",
    "!cd Intro_to_DL_files && caffe train --solver=examples/cifar10/cifar10_quick_solver_cpu.prototxt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each training iteration this time took about 0.8 seconds - approximately forty times slower."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## More information\n",
    "\n",
    "To learn more about these topics, please visit:\n",
    "* GPU accelerated machine learning: [http://www.nvidia.com/object/machine-learning.html](http://www.nvidia.com/object/machine-learning.html)\n",
    "* Caffe: [http://caffe.berkeleyvision.org/](http://caffe.berkeleyvision.org/)\n",
    "* Theano: [http://deeplearning.net/software/theano/](http://deeplearning.net/software/theano/)\n",
    "* Torch: [http://torch.ch/](http://torch.ch/)\n",
    "* DIGITS: [https://developer.nvidia.com/digits](https://developer.nvidia.com/digits)\n",
    "* cuDNN: [https://developer.nvidia.com/cudnn](https://developer.nvidia.com/cudnn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deep Learning Lab Series\n",
    "\n",
    "Make sure to check out the rest of the classes in this Deep Learning lab series.  You can find them [here](https://developer.nvidia.com/deep-learning-courses)."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Raw Cell Format",
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
