# Intro To DL
Ported from
[Introduction to Deep Learning qwiklab](https://nvidia.qwiklab.com/focuses/223).
The lab is meant to help one familiarize oneself with setting up and working
within a container environment.

### Running on DGX-1 within Caffe container

Jupyter is required to run the notebook for this lab. Install Jupyter and lmdb
python module within the caffe container, or setup a virtualenv with Jupyter
and lmdb module, or use whatever is convenient (perhaps Anaconda or Enthought).

#### Creating Caffe container with Jupyter
To install Jupyter and lmbd module within the caffe container follow these
instructions:
```bash
# run the container which will start with root privileges
nvidia-docker run -ti nvcr.io/nvidia/caffe:16.11
# The nvcr.io/nvidia/caffe container is provided with DGX-1 systems. A general
# Caffe container is available from Docker hub nvidia/caffe. 
pip install jupyter lmdb
exit

# list the containers and find the exited container-id
docker ps -a
# create new container image
docker commit <container-id> caffe-with-jupyter
# list images and verify caffe-with-jupyter appears on the list
docker images
# remove the exited container
docker rm <container-id>
```

#### Setup virtualenv with Jupyter

An alternate option is to setup a python virtualenv with Jupyter and lmdb. This
approach avoids having to create a new docker image and could be re-used with
other containers and scenarios. Do this prior to running the container.
```bash
virtualenv  ~/pyenvs/python-with-jupyter
source ~/pyenvs/python-with-jupyter/bin/activate
(python-with-jupyter)$ pip install lmdb
(python-with-jupyter)$ pip install jupyter
# if the above pip install does not work try: pip install -U pip
# Then try installing again.
(python-with-jupyter)$ deactivate
```

### Running the example notebook

If using the new image run the new container via command:
```bash
nvidia-docker run --rm -ti -e HOME=$HOME -v $HOME:$HOME -u $(id -u):$(id -u) -p 8888:8888 caffe-with-jupyter
```

If using the virtualenv run the caffe container via command:
```bash
nvidia-docker run --rm -ti -e HOME=$HOME -v $HOME:$HOME -u $(id -u):$(id -u) -p 8888:8888 nvcr.io/nvidia/caffe:16.11
source ~/penvs/python-with-jupyter/bin/activate
# Caffe container sets PYTHONPATH to /usr/local/python required for Caffe
# python wrapper. Otherwise export PYTHONPATH as:
# export PYTHONPATH+=:/usr/local/python
```

Within the container start the example using the following commands:
```bash
cd /path-to-gitclone/DL_Series
jupyter notebook --no-browser --ip 0.0.0.0
```

Open a browser to view the notebook. The ip address is dependent on the system
setup. In typical intranet setups the Jupyter notebook server can be reached
at the ip address of the host where the container is running, or otherwise
setup an ssh port tunnel between localhost and remote machine.<br/>
Open and run the code in the notebook. Pass the "-gpu comma-sep-list" to caffe
training commands for multi-GPU training.
