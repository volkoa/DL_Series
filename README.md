# DL_Series
Deep Learning series based on qwiklabs. Ported for running in a non-cloud
environment. Refer to official Nvidia [qwiklabs](https://nvidia.qwiklab.com/).

* [Intro To DL](Intro_to_DL) - Ported from
    [Introduction to Deep Learning qwiklab](https://nvidia.qwiklab.com/focuses/223).
    Contains two large binary files (Cifar10 subset in lmdb for training and testing)
    [train data 196 MB](Intro_to_DL/Intro_to_DL_files/examples/cifar10/cifar10_train_lmdb/data.mdb), and 
    [test data 39.3 MB](Intro_to_DL/Intro_to_DL_files/examples/cifar10/cifar10_test_lmdb/data.mdb)
    version controlled via git-lfs.

* [Signal Processing using DIGITS](SignalProcessingWithDIGITS) - Ported from
    [Signal Processing using DIGITS qwiklab](https://nvidia.qwiklab.com/focuses/1578).
    Contains one large binary file with data contents for the lab
    [demo.tar.gz 350M](SignalProcessingWithDIGITS/demo.tar.gz) version
    controlled via git-lfs. Detects chirp signals in spectrograms.

* [Object Detection](ObjectDetection) - Ported from
    [using DIGITS to train an Object Detection network](https://github.com/gheinrich/DIGITS/tree/master/examples/object-detection).
    Detects automotive vehicles in various traffic scenes.

